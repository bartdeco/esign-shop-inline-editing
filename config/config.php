<?php
/**
 * Created by PhpStorm.
 * User: bartdecorte
 * Date: 03/12/2016
 * Time: 09:57
 */

return [

    'admin_login_route' => 'admin.auth.login',

    'translation_edit_route' => 'admin.localization.edit',
    'seo_edit_route' => 'admin.localization.edit',
    'translation_edit_route_parameter' => 'translator',
    'seo_edit_route_parameter' => 'translator',

    'button_labels' => false,
    'color' => 'ec0b43',

    'database' => [
        'table' => 'translations',
        'menu_item_prefix' => 'menu_'
    ],

    'features' => [
        'inline-editing' => true,
        'seo' => true,
        //'mailing' => true
    ]
];