<?php
/**
 * Created by PhpStorm.
 * User: bartdecorte
 * Date: 03/12/2016
 * Time: 09:53
 */

namespace Bartdeco\EsignShopInlineEditing;

use Illuminate\Support\ServiceProvider;

class EsignShopInlineEditingServiceProvider extends ServiceProvider {

    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/config.php' => config_path('esign-shop-inline-editing.php'),
        ], 'config');

        $this->publishes([
            __DIR__ . '/../assets' => public_path('bartdeco/esign-shop-inline-editing'),
        ], 'public');

        if (! $this->app->routesAreCached()) {
            require __DIR__ . '/routes.php';
        }
    }

    public function register()
    {
        $packageConfigFile = __DIR__ . '/../config/config.php';

        $this->mergeConfigFrom(
            $packageConfigFile, 'esign-shop-inline-editing'
        );

        $this->app->singleton(EsignShopInlineEditing::class, function ($app) {
                return new EsignShopInlineEditing();
            }
        );

        $this->app->alias(EsignShopInlineEditing::class, 'esignshopinlineediting');
    }

}