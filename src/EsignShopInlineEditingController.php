<?php
/**
 * Created by PhpStorm.
 * User: bartdecorte
 * Date: 03/12/2016
 * Time: 11:32
 */

namespace Bartdeco\EsignShopInlineEditing;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;

class EsignShopInlineEditingController extends Controller {

    public function enable() {
        session(['esign-shop-inline-editing' => true]);
        // TODO fix
		if (session('_previous')['url'] != Request::url() && !preg_match('/^.*\/admin.*$/', session('_previous')['url'])) {
            return back();
        }
        return redirect('/');
    }

    public function disable() {
        session(['esign-shop-inline-editing' => false]);
        if (session('_previous')['url'] != Request::url() && !preg_match('/^.*\/admin.*$/', session('_previous')['url'])) {
            return back();
        }
        return redirect()->route(config('esign-shop-inline-editing.admin_login_route'));
    }

}