<?php
/**
 * Created by PhpStorm.
 * User: bartdecorte
 * Date: 03/12/2016
 * Time: 10:04
 */

namespace Bartdeco\EsignShopInlineEditing\Facades;

use Bartdeco\EsignShopInlineEditing\EsignShopInlineEditing;
use Illuminate\Support\Facades\Facade;

class InlineEditing extends Facade {

    protected static function getFacadeAccessor()
    {
        return EsignShopInlineEditing::class;
    }

}