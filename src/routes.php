<?php
/**
 * Created by PhpStorm.
 * User: bartdecorte
 * Date: 03/12/2016
 * Time: 11:30
 */

Route::group(['middleware' => ['web']], function () {
    Route::get('inline-editing/enable', ['as' => 'esign-shop-inline-editing.enable', 'uses' => '\Bartdeco\EsignShopInlineEditing\EsignShopInlineEditingController@enable']);
    Route::get('inline-editing/disable', ['as' => 'esign-shop-inline-editing.disable', 'uses' => '\Bartdeco\EsignShopInlineEditing\EsignShopInlineEditingController@disable']);
});