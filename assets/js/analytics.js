/**
 * Created by Bart on 6/02/16.
 */

function Analytics(params) {

	this.clientId = '386775819592-miolc7pl6spor90uh736gcjnb5jf60j8.apps.googleusercontent.com';
	this.scopes = ['https://www.googleapis.com/auth/analytics.readonly'];

	this.gaParams = {
		accountId: null,
		propertyId: null,
		viewId: null
	};

	this.callbacks = {
		success: function(){},
		error: function(){}
	};

	this.init = function(params) {
		var that = this;

		for (var key in that.gaParams) {
			if (that.gaParams.hasOwnProperty(key) && params.hasOwnProperty(key)) {
				that.gaParams[key] = params[key];
			}
		}

		console.log(that.gaParams);

		if (params.hasOwnProperty('success')) {
			that.callbacks.success = params.success;
		}
		if (params.hasOwnProperty('error')) {
			that.callbacks.error = params.error;
		}

		console.log(that.callbacks);

		that.authorize();
	};

	this.authorize = function() {
		var that = this;
		var authData = {
			client_id: that.clientId,
			scope: that.scopes,
			immediate: false
		};

		gapi.auth.authorize(authData, function(response) {
			if (!response.error) {
				that.queryAccounts(that);
			} else {
				that.params.error('Unauthorized');
			}
		});
	};

	this.queryAccounts = function(scope) {
		// Load the Google Analytics client library.
		gapi.client.load('analytics', 'v3').then(function() {

			// Get a list of all Google Analytics accounts for this user
			gapi.client.analytics.management.accounts.list().then(function(response){ scope.handleAccounts(scope, response) });
		});
	};

	this.handleAccounts = function(scope, response) {
		// Handles the response from the accounts list method.
		if (response.result.items && response.result.items.length) {
			// Get the first Google Analytics account.
			console.log(response);
			var match = -1;
			for (var i = 0; i<response.result.items.length; i++) {
				if (response.result.items[i].id == scope.gaParams.accountId) {
					match = i;
				}
			}

			if (match > -1) {
				// Query for properties.
				scope.queryProperties(scope);
			} else {
				// No rights
				scope.callbacks.error('Unauthorized for account');
			}
		} else {
			console.log('No accounts found for this user.');
		}
	};

	this.queryProperties = function(scope) {
		// Get a list of all the properties for the account.
		gapi.client.analytics.management.webproperties.list(
			{'accountId': scope.gaParams.accountId})
			.then(function(response){ scope.handleProperties(response, scope) })
			.then(null, function(err) {
				// Log any errors.
				console.log(err);
			});
	};

	this.handleProperties = function(response, scope) {
		// Handles the response from the webproperties list method.
		if (response.result.items && response.result.items.length) {

			var match = -1;
			for (var i = 0; i<response.result.items.length; i++) {
				if (response.result.items[i].id == scope.gaParams.propertyId) {
					match = i;
				}
			}

			if (match > -1) {
				// Query for Views (Profiles).
				scope.queryProfiles(scope);
			} else {
				// No rights
				scope.callbacks.error('Unauthorized for property');
			}
		} else {
			console.log('No properties found for this user.');
		}
	};

	this.queryProfiles = function(scope) {
		// Get a list of all Views (Profiles) for the first property
		// of the first Account.
		gapi.client.analytics.management.profiles.list({
			'accountId': scope.gaParams.accountId,
			'webPropertyId': scope.gaParams.propertyId
		})
		.then(function(response){ scope.handleProfiles(response, scope); } )
		.then(null, function(err) {
			// Log any errors.
			console.log(err);
		});
	};

	this.handleProfiles = function(response, scope) {
		// Handles the response from the profiles list method.
		if (response.result.items && response.result.items.length) {
			// Get the first View (Profile) ID.
			var match = -1;
			for (var i = 0; i<response.result.items.length; i++) {
				if (response.result.items[i].id == scope.gaParams.viewId) {
					match = i;
				}
			}

			if (match > -1) {
				// Query the Core Reporting API.
				scope.queryCoreReportingApi(scope);
			} else {
				// No rights
				scope.callbacks.error('Unauthorized for view');
			}
		} else {
			console.log('No views (profiles) found for this user.');
		}
	};

	this.queryCoreReportingApi = function(scope) {
		// Query the Core Reporting API for the number sessions for
		// the past seven days.
		gapi.client.analytics.data.ga.get({
			'ids': 'ga:' + scope.gaParams.viewId,
			'start-date': '30daysAgo',
			'end-date': 'today',
			'dimensions': 'ga:date',
			'filters': 'ga:pagePath==/aanbod',
			'metrics': 'ga:pageviews'
		})
		.then(function(response) {
			scope.callbacks.success(response.result);
		})
		.then(null, function(err) {
			// Log any errors.
			console.log(err);
		});
	};

	this.init(params);

}